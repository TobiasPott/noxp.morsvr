# Remark: This repository is deprecated and succeeded by MoRSVR.2 (which will take some time to be public due to 3rd party dependencies and license restrictions)

# Daydream AR  
  
### ARCore & Daydream 6DoF Motion Tracking Project for Unity.  
  
#### Description  
This project is a demo for utilizing Google ARCore and Daydream features to build mobile VR room-scale applications.  
It uses the ARCore tracking features in combination with the Daydream controller for recreating a simple, yet functional approach for room-scale virtual reality.  
  
#### Requirements  
Build:  
Unity3D 2017.2.0f3 (no tests made with any of the patch or beta versions)  
Android SDK with min. API Level 24 (Android 7 or newer)  
  
Run:  
ARCore & Daydream compatible Android device  
> *ARCore device support: https://developers.google.com/ar/discover/*  
> *Daydream device support: https://vr.google.com/daydream/smartphonevr/phones/*  
ARCore Service APK: https://developers.google.com/ar/develop/unity/getting-started#prepare-device  

Additional Hardware:  
Daydream View with ARCore modifications (guide & images and 3D printed parts coming soon)  
> *any other Headset with the possibility to leave the camera of your device uncovered should do. Required by the Daydream runtime check for an active remote*  
Daydream Remote  
> *should come with the Daydream View and is required for extended possibilities for ingame controls and interaction.*  
  
Experimental:  
ARKit compatible iOS device
> *ARKit requires iPhone SE, iPhone 6s or newer or iPad (2017) to run*  
> *Support is for development purposes and needs further integration into the current system*  
  
#### Hardware Modifications  
In order to run the project as an actual room-scale VR experience you need a headset which does not cover up your devices camera.  
Although the devices supporting ARCore are also supporting Daydream you do not need to have a Daydream View.  
Because I don't have access to Google Cardboards or other headsets (Gear etc.) I've modified two Daydream View headsets to uncover Google Pixel and Samsung Galaxy S8 cameras.  
  
The following image shows where you need to cut the caps of your Daydream View cover to clear the camera. (I did it on both side for aesthetic).  
> *Note that it might be possible to leave more space from the hinge to your cut (I've used 6mm) but do not cut closer to the hinge, as some plastic pins keep small metal rods of the hinges in place and those might get loose without.  
> *Note that there are some NFC coils taped to the back of the cover which allow your device to recognize it is inserted in the headset. These fit to my cuts without adjustments but I were required to relocate for cuts made for the Galaxy S8.  
![](Documentation/Remarks_DaydreamView_ModForPixelXL.jpg)  
  
It is much easier to detach the cover from the body although it is kind of a fiddling task.  
It might also a good choice to detach the cover top and bottom parts to precheck the internal placement of NFC, but please be careful as the mounting clips can be easily broken.  
  
If you want to cover up the cut caps you can print some caps I've modeled for myself. They fit on tightly when cut with the given 6mm distance to the hinges.  
[STL for flat caps](Documentation/_3dprint_DaydreamView_ModCap_Generic.stl)  
[STL for Galaxy S8 cap](Documentation/_3dprint_DaydreamView_ModCap_SamsungS8.stl)  
  
  
#### Remarks  
This project refers to Unity Blog's entry:  
Mobile inside-out VR Tracking, now readily available on your phone with Unity - Unity Blog  
> https://blogs.unity3d.com/jp/2017/10/18/mobile-inside-out-vr-tracking-now-readily-available-on-your-phone-with-unity/  
  
  
This project is derived from youten's git repository:  
> https://github.com/youten/ARCoreDream  
More from youten:  
> http://greety.sakura.ne.jp/redo/ (japanese)  


#### Copyrights
Daydream, Cardboard, ARCore, Android and all the other stuff belongs to Google  
All other things I might have missed belong to their respective owners  