﻿using NoXP;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// visual helper for virtual reality camera fade (e.g. used during teleport and other fast positional changes)
/// </summary>
[RequireComponent(typeof(Camera))]
public class AVRCameraFade : MonoBehaviour
{
    public enum CameraFadeTypes
    {
        Black,
        White,
        Vignette
    }

    #region Singleton
    /// <summary>
    /// unsafe singleton instance
    /// </summary>
    private static AVRCameraFade _instance = null;
    /// <summary>
    /// gets the active singleton instance (this is not guaranteed to be not null, separate check is required)
    /// </summary>
    public static AVRCameraFade Instance
    { get { return _instance; } }
    #endregion

    #region Fields
    [SerializeField()]
    private CameraFadeTypes _fadeType = CameraFadeTypes.Black;
    [SerializeField()]
    private Sprite _vignette = null;

    private bool _isFading = false;
    private Camera _camera = null;
    private Coroutine _corFade = null;
    private CanvasGroup _canvasGroupFade = null;

    private Image _imgBlack = null;
    private Image _imgWhite = null;
    private Image _imgVignette = null;

    #endregion 

    #region Properties
    /// <summary>
    /// gets whether a fade is in progress
    /// </summary>
    public bool IsFading
    { get { return _isFading; } }
    #endregion

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != null && _instance != this)
            Debug.LogWarning(this.gameObject.name + " has '" + typeof(AVRCameraFade).Name + "' component attached to it. The component is designed as a singleton and behaviour might not be as expected with multiple instances.");

        _camera = this.GetComponent<Camera>();
        this.CreateFadePanels();
        this.SetFadeType(_fadeType);
    }

    private void CreateFadePanels()
    {
        if (_camera)
        {
            GameObject goCanvasFade = new GameObject("CanvasFade", typeof(Canvas), typeof(CanvasGroup));
            goCanvasFade.SetActive(false);
            goCanvasFade.transform.SetParent(_camera.transform);

            float aspect = 1.25f;
            float vrScale = 1.0f;
            float zPos = _camera.nearClipPlane * 1.1f;
            if (_camera.stereoActiveEye != Camera.MonoOrStereoscopicEye.Mono)
            {
                aspect = (float)Screen.width / (float)Screen.height;
                vrScale = 2.0f;
            }

            goCanvasFade.transform.localPosition = new Vector3(0, 0, zPos);
            goCanvasFade.transform.localScale = new Vector3(zPos * 2, zPos * aspect, zPos) * vrScale;

            _canvasGroupFade = goCanvasFade.GetComponent<CanvasGroup>();
            //_canvasGroupFade.alpha = 0.0f;
            Canvas canvasFade = goCanvasFade.GetComponent<Canvas>();
            canvasFade.renderMode = RenderMode.WorldSpace;
            canvasFade.sortingOrder = 9999; // pull canvas to the very front of rendering (will not  overlay stuff above 9999)
            (canvasFade.transform as RectTransform).sizeDelta = Vector2.one;

            GameObject panelBlack = new GameObject("Black", typeof(Image));
            _imgBlack = panelBlack.GetComponent<Image>();
            _imgBlack.color = Color.black;
            RectTransform panelBlackTfs = panelBlack.transform as RectTransform;
            panelBlackTfs.SetParent(goCanvasFade.transform);
            panelBlackTfs.localPosition = Vector3.zero;
            panelBlackTfs.localRotation = Quaternion.identity;
            panelBlackTfs.localScale = Vector3.one;
            panelBlackTfs.anchorMin = Vector2.zero;
            panelBlackTfs.anchorMax = Vector2.one;
            panelBlackTfs.offsetMin = panelBlackTfs.offsetMax = Vector2.zero;

            GameObject panelWhite = GameObject.Instantiate(panelBlack, goCanvasFade.transform);
            panelWhite.name = "White";
            panelWhite.SetActive(false);
            _imgWhite = panelWhite.GetComponent<Image>();
            _imgWhite.color = Color.white;

            GameObject panelVignette = GameObject.Instantiate(panelBlack, goCanvasFade.transform);
            panelVignette.name = "Vignette";
            panelVignette.SetActive(false);
            _imgVignette = panelVignette.GetComponent<Image>();
            _imgVignette.color = Color.white;
            _imgVignette.sprite = _vignette;
            _imgVignette.type = Image.Type.Simple;
        }
    }

    public void SetFadeType(CameraFadeTypes type)
    {
        _imgBlack.gameObject.SetActive(type == CameraFadeTypes.Black);
        _imgWhite.gameObject.SetActive(type == CameraFadeTypes.White);
        _imgVignette.gameObject.SetActive(type == CameraFadeTypes.Vignette);
    }
    public void Fade(float fadeDuration, float duration, Action callback = null)
    {
        this.CancelFade();
        _corFade = Coroutiner.Start(COR_Fade(fadeDuration, duration, callback));
    }
    public void CancelFade()
    {
        if (_corFade != null)
            Coroutiner.Stop(_corFade);
        _isFading = false;
        _canvasGroupFade.alpha = 0.0f;
        _canvasGroupFade.gameObject.SetActive(false);
    }
    private IEnumerator COR_Fade(float fadeDuration, float duration, Action callback = null)
    {
        _isFading = true;
        _canvasGroupFade.gameObject.SetActive(true);
        float time = 0.0f;
        float invDurationFade = (1.0f / fadeDuration);
        do
        {
            _canvasGroupFade.alpha = time;
            time += Time.deltaTime * invDurationFade;
            yield return null;
        }
        while (time < 1.0f);
        _canvasGroupFade.alpha = 1.0f;

        yield return new WaitForSeconds(duration / 2);
        if (callback != null)
            callback.Invoke();
        yield return new WaitForSeconds(duration / 2);

        time = 0.0f;
        do
        {
            _canvasGroupFade.alpha = 1.0f - time;
            time += Time.deltaTime * invDurationFade;
            yield return null;
        }
        while (time < 1.0f);
        _canvasGroupFade.alpha = 0.0f;
        _canvasGroupFade.gameObject.SetActive(false);
        _isFading = false;
    }



}