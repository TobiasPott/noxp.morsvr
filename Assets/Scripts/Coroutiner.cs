﻿using System.Collections;
using UnityEngine;

namespace NoXP
{

    public class Coroutiner : MonoBehaviour
    {
        private static readonly System.Type TCoroutiner = typeof(Coroutiner);

        private static Coroutiner _instance = null;
        private static Coroutiner Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject goInst = GameObject.Find(TCoroutiner.Name);
                    if (goInst == null)
                        goInst = new GameObject(TCoroutiner.Name, TCoroutiner);
                    goInst.hideFlags = HideFlags.DontSave;
#if !UNITY_EDITOR
                    GameObject.DontDestroyOnLoad(goInst);
#endif
                    _instance = goInst.GetComponent<Coroutiner>();
                }
                return _instance;
            }
        }

        public static Coroutine Start(IEnumerator routine)
        { return Coroutiner.Instance.StartCoroutine(routine); }

        public static void Stop(IEnumerator routine)
        { Coroutiner.Instance.StopCoroutine(routine); }
        public static void Stop(Coroutine routine)
        { Coroutiner.Instance.StopCoroutine(routine); }
        public static void StopAll()
        { Coroutiner.Instance.StopAllCoroutines(); }

    }

}