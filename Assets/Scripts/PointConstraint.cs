﻿using UnityEngine;

public class PointConstraint : MonoBehaviour
{
    public enum UpdateMode
    {
        FixedUpdate,
        Update,
        LateUpdate
    }

    [SerializeField()]
    private UpdateMode _mode = UpdateMode.LateUpdate;

    [Range(0, 1)]
    [SerializeField()]
    private float _blend = 1.0f;
    [SerializeField()]
    private Transform _parentPosition = null;

    private Vector3 _offsetPosition = Vector3.zero;


    private Vector3 TargetPosition
    { get { return _parentPosition.position + _offsetPosition; } }

    private void Awake()
    {
        if (_parentPosition != null)
        {
            _offsetPosition = (this.transform.position - _parentPosition.position);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (_parentPosition != null)
            if (_mode == UpdateMode.Update)
            {
                this.UpdateTransform();
            }
    }
    private void FixedUpdate()
    {
        if (_parentPosition != null)
            if (_mode == UpdateMode.FixedUpdate)
            {
                this.UpdateTransform();
            }
    }
    private void LateUpdate()
    {
        if (_parentPosition != null)
            if (_mode == UpdateMode.LateUpdate)
            {
                this.UpdateTransform();
            }
    }

    private void UpdateTransform()
    {
        if (_blend >= 1.0f)
            this.transform.position = TargetPosition; // _parent.position + (this.transform.rotation * _offsetPosition), _parent.rotation * _offsetRotation);
        else if (_blend > 0.0f)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, TargetPosition, _blend);
        }
    }

}