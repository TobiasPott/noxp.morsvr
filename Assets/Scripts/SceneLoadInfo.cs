﻿using UnityEngine;

public class SceneLoadInfo : MonoBehaviour
{

    [SerializeField()]
    private string _scene;
    [SerializeField()]
    private Vector3 _position = Vector3.zero;
    [SerializeField()]
    private float _fadeDuration = 0.5f;
    [SerializeField()]
    private float _transitionDuration = 1.0f;


    public string Scene
    { get { return _scene; } }
    public Vector3 Position
    { get { return _position; } }
    public float FadeDuration
    { get { return _fadeDuration; } }
    public float TransitionDuration
    { get { return _transitionDuration; } }

}