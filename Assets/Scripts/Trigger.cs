﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Trigger : MonoBehaviour
{

    [SerializeField()]
    private UnityEvent _onEnter = new UnityEvent();
    [SerializeField()]
    private UnityEvent _onStay = new UnityEvent();
    [SerializeField()]
    private UnityEvent _onExit = new UnityEvent();

    [SerializeField()]
    private List<Collider> _included = new List<Collider>();



    private void OnTriggerEnter(Collider other)
    {
        if (_included.Count == 0 || _included.Contains(other))
            this.OnEnter();
    }
    private void OnTriggerStay(Collider other)
    {
        if (_included.Count == 0 || _included.Contains(other))
            this.OnStay();
    }
    private void OnTriggerExit(Collider other)
    {
        if (_included.Count == 0 || _included.Contains(other))
            this.OnExit();
    }


    public void OnEnter()
    {
        if (this._onEnter != null)
            this._onEnter.Invoke();
    }

    public void OnStay()
    {
        if (this._onStay != null)
            this._onStay.Invoke();
    }
    public void OnExit()
    {
        if (this._onExit != null)
            this._onExit.Invoke();
    }
}