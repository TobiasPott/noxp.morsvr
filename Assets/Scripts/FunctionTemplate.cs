﻿using UnityEngine;
using UnityEngine.Events;

public class FunctionTemplate : Function
{
    [SerializeField()]
    private UnityEvent _onPreExecute = new UnityEvent();
    [SerializeField()]
    private UnityEvent _onExecute = new UnityEvent();
    [SerializeField()]
    private UnityEvent _onPostExecute = new UnityEvent();


    protected override void Internal_Pre()
    {
        this.OnPreExecute();
    }
    protected override void Internal_Execute()
    {
        this.OnExecute();
    }
    protected override void Internal_Post()
    {
        this.OnPostExecute();
    }
    protected override void Internal_Clear()
    {
        this.Internal_Post();
    }


    public void OnPreExecute()
    {
        if (_onPreExecute != null)
            _onPreExecute.Invoke();
    }
    public void OnExecute()
    {
        if (_onExecute != null)
            _onExecute.Invoke();
    }
    public void OnPostExecute()
    {
        if (_onPostExecute != null)
            _onPostExecute.Invoke();
    }
}
