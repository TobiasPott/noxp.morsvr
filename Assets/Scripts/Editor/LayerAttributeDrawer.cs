﻿// The property drawer class should be placed in an editor script, inside a folder called Editor.

// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(LayerAttribute))]
public class LayerAttributeDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.Integer)
            // First get the attribute since it contains the range for the slider
            //LayerAttribute range = attribute as LayerAttribute;
            property.intValue = EditorGUI.LayerField(position, label, property.intValue);
        else
            EditorGUI.LabelField(position, label, new GUIContent("'Layer' attribute is only valid for integers."));
    }
}