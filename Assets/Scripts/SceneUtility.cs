﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneUtility : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void LoadSceneFaded(string sceneName)
    {
        AVRCameraFade.Instance.Fade(0.5f, 1.0f, () => { SceneManager.LoadScene(sceneName, LoadSceneMode.Single); });
    }

    public void LoadScene(SceneLoadInfo info)
    {
        AVRCameraFade.Instance.Fade(info.FadeDuration, info.TransitionDuration, () =>
        {
            AVRTrackingRoot.Instance.transform.position = info.Position;
            SceneManager.LoadScene(info.Scene, LoadSceneMode.Single);
        });
    }

    public void Log(string message)
    {
        Debug.Log(message);
    }

    public static void StaticLoadScene(string scene, Vector3 position, float fade, float transition, Action callback = null)
    {
        AVRCameraFade.Instance.Fade(fade, transition, () =>
        {
            AVRTrackingRoot.Instance.transform.position = position;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
            if (callback != null)
                callback.Invoke();
        });
    }


}
