﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(GvrLaserPointer), typeof(GvrLaserVisual), typeof(LineRenderer))]
public class LaserPointer : Function
{

    private GvrLaserPointer _gvrPointer;
    private GvrLaserVisual _gvrVisual;
    private LineRenderer _lineRenderer;
    private GvrControllerReticleVisual _gvrRecticleVisual;

    private void Awake()
    {
        _gvrPointer = this.GetComponent<GvrLaserPointer>();
        _gvrVisual = this.GetComponent<GvrLaserVisual>();
        _lineRenderer = this.GetComponent<LineRenderer>();
        _gvrRecticleVisual = _gvrVisual.reticle;
    }

    // Update is called once per frame
    protected override void Update()
    {
        // call to AVRFunction.Update();
        base.Update();

    }


    protected override void Internal_Pre()
    {
        _gvrPointer.enabled = true;
        _gvrVisual.enabled = true;
        _lineRenderer.enabled = true;
        _gvrRecticleVisual.gameObject.SetActive(true);
    }

    protected override void Internal_Execute()
    {
    }

    protected override void Internal_Post()
    {
        _gvrPointer.enabled = false;
        _gvrVisual.enabled = false;
        _lineRenderer.enabled = false;
        _gvrRecticleVisual.gameObject.SetActive(false);
    }
    protected override void Internal_Clear()
    {
        this.Internal_Post();
    }



}