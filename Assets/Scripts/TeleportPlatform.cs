﻿using UnityEngine;

public class TeleportPlatform : MonoBehaviour
{

    [SerializeField()]
    private bool _isActive = true;
    [SerializeField()]
    private Material _materialActive = null;
    [SerializeField()]
    private Material _materialInactive = null;

    private Renderer _modelRenderer = null;
    private Collider _trigger = null;


    protected Renderer ModelRenderer
    {
        get
        {
            if (_modelRenderer == null)
                _modelRenderer = this.transform.Find("Models").Find("LightCylinder").GetComponent<Renderer>();
            return _modelRenderer;
        }
    }

    protected Collider Trigger
    {
        get
        {
            if (_trigger == null)
                _trigger = this.transform.Find("Trigger").GetComponent<Collider>();
            return _trigger;
        }
    }

    public bool IsActive
    {
        get { return _isActive; }
        set
        {
            _isActive = true;
            this.ModelRenderer.sharedMaterial = _isActive ? _materialActive : _materialInactive;
            this.Trigger.enabled = _isActive;
        }
    }

    private void Awake()
    {
        this.ModelRenderer.sharedMaterial = _isActive ? _materialActive : _materialInactive;
        this.Trigger.enabled = _isActive;
    }

}