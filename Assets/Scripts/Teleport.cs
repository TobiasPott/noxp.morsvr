﻿using NoXP;
using System;
using System.Collections;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(TeleportArc))]
public class Teleport : Function
{

    [Header("Teleport")]
    [SerializeField()]
    private LayerMask _traceLayerMask = (LayerMask)1 << 1;
    [SerializeField()]
    private float _arcDistance = 10.0f;
    [SerializeField()]
    private Transform _markerPrefab = null;


    [SerializeField()]
    private float _teleportFade = 0.05f;
    [SerializeField()]
    private float _teleportDuration = 0.2f;


    [Header("Color")]
    [SerializeField()]
    private string _colorPropertyName = "_TintColor";
    [SerializeField()]
    private Color _pointerColor = new Color(0.0f, 1.0f, 0.5f);
    [SerializeField()]
    private Color _pointerColorInvalid = new Color(1.0f, 0.0f, 0.0f);


    private bool _isTeleporting = false;
    private Transform _marker = null;
    private bool _arcVisible = false;
    private TeleportArc _arc = null;
    private bool _hitSomething = false;
    private RaycastHit _hitInfo;
    private Coroutine _corTeleportBlock = null;


    public Transform Marker
    {
        get
        {
            if (_marker == null)
            {
                _marker = GameObject.Instantiate(_markerPrefab);
                _marker.transform.parent = AVRTrackingRoot.Instance.transform;
            }
            return _marker;
        }
    }

    private void Awake()
    {
        _arc = this.GetComponent<TeleportArc>();
        _arc.traceLayerMask = _traceLayerMask;
    }

    protected override void Update()
    {
        // call to AVRFunction.Update();
        base.Update();

        if (_arcVisible)
            this.UpdateTeleportArc();


#if UNITY_EDITOR
        // DEBUG
        if (Input.GetKeyDown(KeyCode.Alpha1))
            this.ShowTrace();
        if (Input.GetKeyDown(KeyCode.Alpha2))
            this.HideTrace();
        if (Input.GetKeyDown(KeyCode.T))
            this.DoTeleport();
        // END DEBUG
#endif
    }

    protected override void Internal_Pre()
    {
        this.ShowTrace();
    }

    protected override void Internal_Execute()
    {
        this.DoTeleport();
    }

    protected override void Internal_Post()
    {
        this.HideTrace();
    }

    protected override void Internal_Clear()
    {
        this.Internal_Post();
    }


    public void ShowTrace()
    {
        _arc.Show();
        _arcVisible = true;
        Marker.gameObject.SetActive(true);
    }
    public void HideTrace()
    {
        _arc.Hide();
        _arcVisible = false;
        Marker.gameObject.SetActive(false);
    }

    public void SetMarkerColor(Color color)
    {
        // might change this to iteration over all renderers in marker hierarchy when non-default markers are used
        Marker.GetComponent<Renderer>().sharedMaterial.SetColor(_colorPropertyName, color);
    }
    public void DoTeleport(Transform tfs, Vector3 targetPosition)
    {
        if (!_isTeleporting)
        {
            this.TeleportBlock(delegate () { tfs.position = targetPosition; });
            if (AVRCameraFade.Instance != null)
            {
                AVRCameraFade.Instance.CancelFade();
                AVRCameraFade.Instance.Fade(_teleportFade, _teleportDuration);
            }
        }
    }
    public void DoTeleport(Transform target)
    {
        if (!_isTeleporting && AVRTrackingRoot.Instance != null && target != null)
            DoTeleport(AVRTrackingRoot.Instance.transform, target.position);
    }
    public void DoTeleport(Vector3 targetPosition)
    {
        if (!_isTeleporting && AVRTrackingRoot.Instance != null)
            DoTeleport(AVRTrackingRoot.Instance.transform, targetPosition);
    }
    public void DoTeleport()
    {
        if (_hitSomething && !_isTeleporting && AVRTrackingRoot.Instance != null)
            DoTeleport(AVRTrackingRoot.Instance.transform, _hitInfo.point);
    }

    private void UpdateTeleportArc()
    {
        bool hitChanged = false;
        Vector3 pointerDir = this.transform.forward;
        //Check pointer angle
        float dotUp = Vector3.Dot(pointerDir, Vector3.up);
        bool pointerAtBadAngle = false;
        if (dotUp > 0.75f)
        {
            pointerAtBadAngle = true;
            hitChanged = true;
            _hitSomething = false;
        }
        //Trace to see if the pointer hit anything
        _arc.SetArcData(this.transform.position, pointerDir * _arcDistance, true, pointerAtBadAngle);
        Vector3 hitPosition = _arc.GetArcPositionAtTime(_arc.arcDuration);
        if (_arc.DrawArc(out _hitInfo))
        {
            if (!hitChanged)
            {
                hitPosition = _hitInfo.point + _hitInfo.normal * 0.05f;

                RaycastHit hitInfoFloor;
                if (Physics.Raycast(hitPosition + (0.1f * Vector3.up), -Vector3.up, out hitInfoFloor, 0.25f, ~_traceLayerMask))
                {
                    hitPosition = hitInfoFloor.point;
                    if (_hitSomething) hitChanged = true;
                    _hitSomething = false;
                }
                else if (Physics.Raycast(hitPosition + (0.1f * Vector3.up), -Vector3.up, out hitInfoFloor, 0.25f, _traceLayerMask))
                {
                    hitPosition = hitInfoFloor.point;
                    if (!_hitSomething) hitChanged = true;
                    _hitSomething = true;
                }
                else
                {
                    if (_hitSomething) hitChanged = true;
                    _hitSomething = false;
                }
            }
        }
        else
        {
            if (_hitSomething) hitChanged = true;
            _hitSomething = false;
        }

        // process only once when hit state changed from false => true or vice versa
        if (hitChanged)
        {
            if (_hitSomething)
                this.SetMarkerColor(_pointerColor);
            else
                this.SetMarkerColor(_pointerColorInvalid);
        }

        this.Marker.gameObject.SetActive(!pointerAtBadAngle);
        // process always for the specific hit state 
        if (_hitSomething)
        {
            this.Marker.position = hitPosition;
            _arc.SetColor(_pointerColor);
        }
        else
        {
            //this.Marker.position = _arc.GetArcPositionAtTime(_arc.arcDuration);
            this.Marker.position = hitPosition;
            _arc.SetColor(_pointerColorInvalid);
        }
    }

    private void CancelTeleportBlock()
    {

        if (_corTeleportBlock != null)
            Coroutiner.Stop(_corTeleportBlock);
        _corTeleportBlock = null;
    }
    private void TeleportBlock(Action callback = null)
    {
        this.CancelTeleportBlock();
        _corTeleportBlock = Coroutiner.Start(this.COR_TeleportBlock(callback));
    }
    private IEnumerator COR_TeleportBlock(Action callback = null)
    {
        _isTeleporting = true;
        yield return new WaitForSeconds(_teleportFade);
        yield return new WaitForSeconds(_teleportDuration / 2);
        if (callback != null)
            callback.Invoke();
        yield return new WaitForSeconds(_teleportDuration / 2);
        yield return new WaitForSeconds(_teleportFade);
        _isTeleporting = false;
    }

}