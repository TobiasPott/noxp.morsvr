﻿using UnityEngine;
using UnityEngine.XR;

public class AVRTrackingRoot : MonoBehaviour
{
    // ! ! ! !
    // iOS build instruction/modifications
    // https://answers.unity.com/questions/1333075/project-runs-in-unity-but-fails-when-building-for.html
    // https://answers.unity.com/questions/985308/googlecardboard-app-refuses-to-work-for-ios-build.html
    // 

    private const float ResetTimeout = 1.0f;



    private static AVRTrackingRoot _instance = null;
    private static GvrTrackedController _controller = null;


    public static AVRTrackingRoot Instance
    { get { return _instance; } }
    public static GvrTrackedController Controller
    { get { return _controller; } }


    private Camera _camera = null;
    private float _pressResetInit = -1.0f;
    private bool _pressReset = false;


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            GameObject.DontDestroyOnLoad(this.gameObject);
            _camera = this.GetComponentInChildren<Camera>();
            _controller = this.GetComponentInChildren<GvrTrackedController>(true);
        }
        else if (_instance != null && _instance != this)
        {
            Debug.LogWarning(this.gameObject.name + " has '" + typeof(AVRTrackingRoot).Name + "' component attached to it. The component is designed as a singleton and behaviour might not be as expected with multiple instances.");
            GameObject.Destroy(this.gameObject);
        }
    }


    public void InitPressReset()
    {
        if (XRSettings.loadedDeviceName.Equals("cardboard") || Application.isEditor)
        {
            _pressReset = true;
            _pressResetInit = Time.time + ResetTimeout;
        }
    }

    public void UpdatePressReset()
    {
        if (_pressReset)
        {
            if (Time.time >= _pressResetInit)
            {
                Debug.Log("Do ResetView");
                _pressResetInit = Time.time;
                _pressReset = false;
                this.ResetView();
            }
        }
    }

    public void ResetView()
    {
        if (_camera == null)
            _camera = this.GetComponentInChildren<Camera>();
        Vector3 eulerAngles = _camera.transform.eulerAngles;
        this.transform.rotation = Quaternion.Inverse(Quaternion.Euler(0, eulerAngles.y, 0));
        _camera.transform.eulerAngles = eulerAngles;
    }


    private void Update()
    {
        //if (GoogleVRInput.GetButtonDown(AVRInput.Button.Home))
        //    this.InitPressReset();
        //if (GoogleVRInput.GetButtonUp(AVRInput.Button.Home))
        //    this.UpdatePressReset();
    }


}