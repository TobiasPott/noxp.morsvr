﻿using System;
using UnityEngine;

public abstract class Function : MonoBehaviour
{

    [Header("Input")]
    [SerializeField()]
    private FilteredButton _buttonPre = new FilteredButton(MoRSVRInputBase.Button.TrackpadTouch, MoRSVRInputBase.ButtonState.Down, FilteredButton.AreaPreset.Center);
    [SerializeField()]
    private FilteredButton _buttonExecute = new FilteredButton(MoRSVRInputBase.Button.TrackpadPress, MoRSVRInputBase.ButtonState.Up, FilteredButton.AreaPreset.Center);
    [SerializeField()]
    private FilteredButton _buttonPost = new FilteredButton(MoRSVRInputBase.Button.TrackpadTouch, MoRSVRInputBase.ButtonState.Up, FilteredButton.AreaPreset.Center);


    protected virtual void OnEnable()
    {
        _buttonPre.UpdateAreaPreset();
        _buttonExecute.UpdateAreaPreset();
        _buttonPost.UpdateAreaPreset();
    }
    protected virtual void OnDisable()
    {
        this.Clear();
    }

    protected virtual void Update()
    {
        if (this.enabled)
        {
            this.Pre();
            this.Execute();
            this.Post();
        }
    }


    public virtual void Pre()
    {
        if (_buttonPre.HandleButton())
            this.Internal_Pre();
    }
    public virtual void Execute()
    {
        if (_buttonExecute.HandleButton())
            this.Internal_Execute();
    }
    public virtual void Post()
    {
        if (_buttonPost.HandleButton())
            this.Internal_Post();
    }

    public virtual void Clear()
    {
        this.Internal_Clear();
    }


    private void HandleButton(MoRSVRInputBase.Button button, MoRSVRInputBase.ButtonState state, Action func)
    {
        switch (state)
        {
            case MoRSVRInputBase.ButtonState.Down:
                if (MoRSVRInput.GetButtonDown(button)) func.Invoke();
                break;
            case MoRSVRInputBase.ButtonState.Hold:
                if (MoRSVRInput.GetButton(button)) func.Invoke();
                break;
            case MoRSVRInputBase.ButtonState.Up:
                if (MoRSVRInput.GetButtonUp(button)) func.Invoke();
                break;
        }
    }

    protected abstract void Internal_Pre();
    protected abstract void Internal_Execute();
    protected abstract void Internal_Post();
    protected abstract void Internal_Clear();

}
