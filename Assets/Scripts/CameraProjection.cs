﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraProjection : MonoBehaviour
{
    [SerializeField()]
    private LayerMask _cullingMask = -1;
    [SerializeField()]
    private Shader _replacementShader = null;

    private Camera _camera;

    private void Awake()
    {
        _camera = this.GetComponent<Camera>();
        _camera.cullingMask = _cullingMask;
    }

    private void OnEnable()
    {
        _camera.SetReplacementShader(_replacementShader, string.Empty);
    }

    private void OnDisable()
    {
        _camera.ResetReplacementShader();
    }

}