﻿//-----------------------------------------------------------------------
// <copyright file="HelloARController.cs" company="Google">
//
// Copyright 2017 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------
// modified https://blogs.unity3d.com/jp/2017/10/18/mobile-inside-out-vr-tracking-now-readily-available-on-your-phone-with-unity/

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;
using System;
using UnityEngine.Events;
using UnityEngine.XR;

namespace NoXP.DaydreamAR
{
    [Serializable()]
    public class AVRTrackingEvent : UnityEvent<AVRTrackingController>
    { }

    /// <summary>
    /// Controlls the HelloAR example.
    /// </summary>
    public class AVRTrackingController : MonoBehaviour
    {
        public Vector3 m_scaleFactor = new Vector3(2, 2, 2);

        private bool _trackingStarted = false;

        private Vector3 _prevARPosePosition;
        private Quaternion _arPoseRecenterOffset = Quaternion.identity;

        private int _numberOfTrackedPlanes = 0;
        private int _numberOfPrevTrackedPlanes = 0;
        private List<TrackedPlane> m_trackedPlanes = new List<TrackedPlane>();
        private List<TrackedPlane> m_trackables = new List<TrackedPlane>();

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
        /// </summary>
        private bool m_IsQuitting = false;


        public int NumberOfTrackedPlanes
        { get { return _numberOfTrackedPlanes; } }

        [SerializeField()]
        private AVRTrackingEvent _onTrackingRestored = new AVRTrackingEvent();
        [SerializeField()]
        private AVRTrackingEvent _onTrackingLost = new AVRTrackingEvent();

        private bool _isTracking = false;

        [Header("Debug")]
        [SerializeField()]
        private bool _useIndicator = false;
        [SerializeField()]
        private GameObject _indicatorPrefab = null;
        private GameObject _indicatorPose = null;
        private GameObject _indicatorSelf = null;


        private void Awake()
        {
            // Unity defaults to targeting 30fps on Android which makes the color image look stuttery since it does
            // not come in at a perfect every 33ms and so every so often misses a frame. Target an extremely high
            // frame rate so that Unity instead updates every vblank.
            Application.targetFrameRate = 90;
            QualitySettings.vSyncCount = 0;
            CheckCameraPermission();


            //_trackedPose = this.GetComponent<TrackedPoseDriver>();
            InputTracking.trackingAcquired += InputTracking_TrackingAcquired;
            InputTracking.trackingLost += InputTracking_TrackingLost;

            // DEBUG
            if (_useIndicator)
            {
                _indicatorPose = CreateIndicator(_indicatorPrefab, Color.yellow);
                _indicatorPose.transform.rotation = Quaternion.Euler(0, Frame.Pose.rotation.eulerAngles.y, 0);
                _indicatorPose.transform.position = new Vector3(this.transform.position.x, 0, this.transform.position.z);
                _indicatorSelf = CreateIndicator(_indicatorPrefab, Color.green);
                _indicatorSelf.transform.rotation = this.transform.rotation;
                _indicatorSelf.transform.position = new Vector3(this.transform.position.x, 0, this.transform.position.z);
            }
        }

        private void InputTracking_TrackingLost(XRNodeState obj)
        {
            _isTracking = false;
        }

        private void InputTracking_TrackingAcquired(XRNodeState obj)
        {
            _isTracking = true;
        }

        public void Update()
        {
            _QuitOnConnectionErrors();

            // AR frame pose
            if (_indicatorPose != null)
            {
                _indicatorPose.transform.rotation = Quaternion.Euler(0, Frame.Pose.rotation.eulerAngles.y, 0);
                _indicatorPose.transform.position = new Vector3(this.transform.position.x, 0, this.transform.position.z);
            }


            if (_isTracking)
            {
                // update frame tracking state changes
                this.CheckFrameTracking();
                // update tracked planes and related fields
                this.CheckTrackedPlanes();
            }

            if (_numberOfTrackedPlanes != 0)
            {
                if (GvrControllerInput.Recentered)
                    _arPoseRecenterOffset = Quaternion.Euler(0, this.transform.rotation.eulerAngles.y - Frame.Pose.rotation.eulerAngles.y, 0);


                Screen.sleepTimeout = SleepTimeout.NeverSleep;
                Vector3 currentARPosition = Frame.Pose.position;

                if (!_trackingStarted)
                {
                    _trackingStarted = true;
                    _prevARPosePosition = Frame.Pose.position;
                }

                //Remember the previous position so we can apply deltas
                Vector3 deltaPosition = currentARPosition - _prevARPosePosition;
                _prevARPosePosition = currentARPosition;

                if (AVRTrackingRoot.Instance != null)
                {
                    Vector3 scaledTranslation = new Vector3(m_scaleFactor.x * deltaPosition.x, m_scaleFactor.y * deltaPosition.y, m_scaleFactor.z * deltaPosition.z);
                    AVRTrackingRoot.Instance.transform.Translate(_arPoseRecenterOffset * scaledTranslation);
                }

            }

        }

        private void CheckFrameTracking()
        {
            m_trackables.Clear();
            Session.GetTrackables<TrackedPlane>(m_trackables, TrackableQueryFilter.All);
            bool anyTracking = false;
            for (int i = 0; i < m_trackables.Count; i++)
            {
                if (m_trackables[i].TrackingState == TrackingState.Tracking)
                {
                    anyTracking = true;
                    break;
                }

            }
            if (!anyTracking)
            {
                _trackingStarted = false;  // if tracking lost or not initialized
                const int LOST_TRACKING_SLEEP_TIMEOUT = 15;
                Screen.sleepTimeout = LOST_TRACKING_SLEEP_TIMEOUT;
                return;
            }

        }
        private void CheckTrackedPlanes()
        {
            m_trackedPlanes.Clear();
            Session.GetTrackables<TrackedPlane>(m_trackedPlanes, TrackableQueryFilter.All);
            //Frame.GetAllPlanes(ref m_trackedPlanes);
            _numberOfPrevTrackedPlanes = _numberOfTrackedPlanes;
            _numberOfTrackedPlanes = m_trackedPlanes.Count;


            if (/*_numberOfTrackedPlanes == 0 && */_numberOfTrackedPlanes < _numberOfPrevTrackedPlanes)
            {
                //_trackedPose.trackingType = TrackedPoseDriver.TrackingType.RotationOnly;
                this.OnTrackingLost();
            }
            else if (_numberOfTrackedPlanes > 0 && _numberOfTrackedPlanes != _numberOfPrevTrackedPlanes)
            {
                //_trackedPose.trackingType = TrackedPoseDriver.TrackingType.RotationAndPosition;
                //_anchor = Session.CreateAnchor(Frame.Pose);
                //GameObject.DontDestroyOnLoad(_anchor.gameObject);
                //this.transform.parent = _anchor.transform;
                this.OnTrackingRestored();
            }

        }

        private Anchor _anchor = null;

        /// <summary>
        /// Quit the application if there was a connection error for the ARCore session.
        /// </summary>
        private void _QuitOnConnectionErrors()
        {
            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }

        private void CheckCameraPermission()
        {
            const string ANDROID_CAMERA_PERMISSION_NAME = "android.permission.CAMERA";
            // Create an asynchronous task for the potential permissions flow and service connection.
            // Attempt service connection immediately if permissions are granted.
            if (!AndroidPermissionsManager.IsPermissionGranted(ANDROID_CAMERA_PERMISSION_NAME))
            {

                // Request needed permissions and attempt service connection if granted.
                AndroidPermissionsManager.RequestPermission(ANDROID_CAMERA_PERMISSION_NAME).ThenAction((requestResult) =>
                {
                    //if (!requestResult.IsAllGranted)  
                    //{
                    //m_camPoseText.text = "ARCore cannot be used due to missing camera permissions.";
                    //ARDebug.LogError("ARCore connection failed because a needed permission was rejected.");
                    //SessionManager.ConnectionState = SessionConnectionState.UserRejectedNeededPermission;
                    //onTaskComplete(SessionManager.ConnectionState);
                    //}
                });
            }
        }

        public void OnTrackingRestored()
        {
            if (_onTrackingRestored != null)
                _onTrackingRestored.Invoke(this);
        }
        public void OnTrackingLost()
        {
            if (_onTrackingLost != null)
                _onTrackingLost.Invoke(this);
        }


        private GameObject CreateIndicator(GameObject prefab, Color color)
        {
            if (prefab != null)
            {
                GameObject indicator = GameObject.Instantiate(prefab);
                indicator.transform.parent = this.transform;
                indicator.transform.SetPositionAndRotation(this.transform.position, this.transform.rotation);

                // apply material
                Material mat = new Material(Shader.Find("Standard"));
                mat.SetColor("_Color", color);
                Renderer[] renderers = indicator.GetComponentsInChildren<Renderer>(true);
                foreach (Renderer r in renderers)
                    r.sharedMaterial = mat;

                return indicator;
            }
            return null;
        }

    }
}
