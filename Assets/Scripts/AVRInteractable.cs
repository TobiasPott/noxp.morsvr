﻿using UnityEngine;


public class AVRInteractable : MonoBehaviour
{
    public enum Types
    {
        UI,
        Physics
    }

    [SerializeField()]
    private Types _type = Types.UI;

    private Collider _collider = null;
    private Canvas _canvas = null;


    public Types Type
    { get { return _type; } }
    public Canvas Canvas
    {
        get
        {
            if (_canvas == null)
                _canvas = this.GetComponentInParent<Canvas>();
            return _canvas;
        }
    }
    public Collider Collider
    {
        get
        {
            if (_collider == null)
                _collider = this.GetComponent<Collider>();
            return _collider;
        }
    }


    private void Awake()
    {
        _collider = this.GetComponent<Collider>();
        if (_type == Types.Physics)
        {
            if (_collider == null)
                Debug.LogWarning("AVRInteractable used with Physics requires a collider to work properly. " + this.name + " does not have one, please check your setup.");
        }
        else if (_type == Types.UI)
        {
            _canvas = this.GetComponentInParent<Canvas>();
            if (_canvas == null)
                Debug.LogWarning("AVRInteractable used with UI requires a canvas in it's parent hierarchy to work properly. " + this.name + " does not have one, please check your setup.");
            if (_collider == null)
            {
                RectTransform rectTfs = this.transform as RectTransform;
                Vector2 size = rectTfs.sizeDelta;
                Vector2 pivotOffset = new Vector2(0.5f, 0.5f) - rectTfs.pivot;

                BoxCollider boxCol = this.gameObject.AddComponent<BoxCollider>();
                _collider = boxCol;
                boxCol.size = new Vector3(size.x, size.y, 0.1f);
                boxCol.center = new Vector3(size.x * pivotOffset.x, size.y * pivotOffset.y, 0);
            }
        }

    }

    private void OnTransformParentChanged()
    {
        Canvas newCanvas = this.GetComponentInParent<Canvas>();
        if (newCanvas != _canvas)
            _canvas = newCanvas;
    }

}