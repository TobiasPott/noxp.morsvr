﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class AVRInput
{

}

[Serializable()]
public class FilteredButton
{
    public enum AreaPreset
    {
        None,
        // sides & half-sides
        Left,
        LeftCenter,
        LeftHalf,
        LeftCenterHalf,
        Right,
        RightCenter,
        RightHalf,
        RightCenterHalf,
        Top,
        TopCenter,
        TopHalf,
        TopCenterHalf,
        Bottom,
        BottomCenter,
        BottomHalf,
        BottomCenterHalf,
        // center & full area
        Center,
        CenterHorizontal,
        CenterVertical,
        Full,
        // corner definitions
        TopLeft,
        TopRight,
        TopLeftHalf,
        TopRightHalf,
        BottomLeft,
        BottomRight,
        BottomLeftHalf,
        BottomRightHalf,
        // custom/manual setup
        Custom
    }


    [SerializeField()]
    private MoRSVRInput.Button _button;
    [SerializeField()]
    private MoRSVRInput.ButtonState _state = MoRSVRInput.ButtonState.Down;
    [SerializeField()]
    private AreaPreset _preset = AreaPreset.None;
    [SerializeField()]
    private bool _filterTouchpad = false;
    [Tooltip("Filter area in the range of (-1, -1; top-left) to (1, 1; bottom-right).")]
    [SerializeField()]
    private Rect _filterTouchpadArea = new Rect(-1, -1, 2, 2);

    public FilteredButton(MoRSVRInput.Button button, MoRSVRInput.ButtonState state)
    {
        _button = button;
        _state = state;
        this.SetAreaPreset(AreaPreset.None);
    }

    public FilteredButton(MoRSVRInput.Button button, MoRSVRInput.ButtonState state, AreaPreset preset)
    {
        _button = button;
        _state = state;
        this.SetAreaPreset(preset);
    }

    public void UpdateAreaPreset()
    {
        this.SetAreaPreset(_preset);
    }
    private void SetAreaPreset(AreaPreset preset)
    {
        _preset = preset;
        _filterTouchpad = true;
        switch (preset)
        {
            // sides & half-sides
            case AreaPreset.Left:
                _filterTouchpadArea.Set(-1, -1, 1, 2);
                break;
            case AreaPreset.LeftCenter:
                _filterTouchpadArea.Set(-1, -0.5f, 1, 1);
                break;
            case AreaPreset.LeftHalf:
                _filterTouchpadArea.Set(-1, -1, 0.5f, 2);
                break;
            case AreaPreset.LeftCenterHalf:
                _filterTouchpadArea.Set(-1, -0.5f, 0.5f, 1);
                break;
            case AreaPreset.Right:
                _filterTouchpadArea.Set(0, -1, 1, 2);
                break;
            case AreaPreset.RightCenter:
                _filterTouchpadArea.Set(0, -0.5f, 1, 1);
                break;
            case AreaPreset.RightHalf:
                _filterTouchpadArea.Set(0.5f, -1, 0.5f, 2);
                break;
            case AreaPreset.RightCenterHalf:
                _filterTouchpadArea.Set(0.5f, -0.5f, 0.5f, 1);
                break;
            case AreaPreset.Top:
                _filterTouchpadArea.Set(-1, -1, 2, 1);
                break;
            case AreaPreset.TopCenter:
                _filterTouchpadArea.Set(-0.5f, -1, 1, 1);
                break;
            case AreaPreset.TopHalf:
                _filterTouchpadArea.Set(-1, -1, 2, 0.5f);
                break;
            case AreaPreset.TopCenterHalf:
                _filterTouchpadArea.Set(-0.5f, -1, 1, 0.5f);
                break;
            case AreaPreset.Bottom:
                _filterTouchpadArea.Set(-1, 0, 2, 1);
                break;
            case AreaPreset.BottomCenter:
                _filterTouchpadArea.Set(-0.5f, 0, 1, 1);
                break;
            case AreaPreset.BottomHalf:
                _filterTouchpadArea.Set(-1, 0.5f, 2, 0.5f);
                break;
            case AreaPreset.BottomCenterHalf:
                _filterTouchpadArea.Set(-0.5f, 0.5f, 1, 0.5f);
                break;
            // center & full
            case AreaPreset.Full:
                _filterTouchpadArea.Set(-1, -1, 2, 2);
                break;
            case AreaPreset.Center:
                _filterTouchpadArea.Set(-0.5f, -0.5f, 1, 1);
                break;
            case AreaPreset.CenterHorizontal:
                _filterTouchpadArea.Set(-1, -0.5f, 2, 1);
                break;
            case AreaPreset.CenterVertical:
                _filterTouchpadArea.Set(-0.5f, -1, 1, 2);
                break;
            // corners
            case AreaPreset.TopLeft:
                _filterTouchpadArea.Set(-1, -1, 1, 1);
                break;
            case AreaPreset.TopRight:
                _filterTouchpadArea.Set(0, -1, 1, 1);
                break;
            case AreaPreset.TopLeftHalf:
                _filterTouchpadArea.Set(-1, -1, 0.5f, 0.5f);
                break;
            case AreaPreset.TopRightHalf:
                _filterTouchpadArea.Set(0.5f, -1, 0.5f, 0.5f);
                break;
            case AreaPreset.BottomLeft:
                _filterTouchpadArea.Set(-1, 0, 1, 1);
                break;
            case AreaPreset.BottomRight:
                _filterTouchpadArea.Set(0, 0, 1, 1);
                break;
            case AreaPreset.BottomLeftHalf:
                _filterTouchpadArea.Set(-1, 0.5f, 0.5f, 0.5f);
                break;
            case AreaPreset.BottomRightHalf:
                _filterTouchpadArea.Set(0.5f, 0.5f, 0.5f, 0.5f);
                break;


            case AreaPreset.None:
                _filterTouchpad = false;
                _filterTouchpadArea.Set(-1, -1, 1, 1);
                break;
        }
    }


    public bool HandleButton()
    {
        if ((_button == MoRSVRInput.Button.TrackpadPress || _button == MoRSVRInput.Button.TrackpadTouch)
            && _filterTouchpad && _filterTouchpadArea.Contains(MoRSVRInput.Trackpad))
        {
            switch (_state)
            {
                case MoRSVRInput.ButtonState.Down:
                    return MoRSVRInput.GetButtonDown(_button);
                case MoRSVRInput.ButtonState.Hold:
                    return MoRSVRInput.GetButton(_button);
                case MoRSVRInput.ButtonState.Up:
                    return MoRSVRInput.GetButtonUp(_button);
            }
        }
        return false;
    }
}



// ! ! ! !
// change the single fixed DaydreamInput class to allow different types to be selected as input source
//  ->  e.g. Daydream, Remote Device (2nd Smartphone/Tablet)


public abstract class MoRSVRInputBase
{
    /// <summary>
    /// enumeration of all buttons available on a Google Daydream remote
    /// </summary>
    public enum Button
    {
        /// <summary>
        /// the application button (the round one with the elevated line on it)
        /// </summary>
        App, // = 336, //(int)KeyCode.JoystickButton6,
        /// <summary>
        /// the home button (the indented one, used for recentering or return to daydream hub)
        /// </summary>
        Home, // = 337, // (int)KeyCode.JoystickButton7,
        /// <summary>
        /// the valume up button (cannot be unbound from controlling the system audio level)
        /// </summary>
        VolumeUp, // = 330, //(int)KeyCode.JoystickButton0,
        /// <summary>
        /// the volume down button (cannot be unbound from controlling the system audio level)
        /// </summary>
        VolumeDown, // = 331, //(int)KeyCode.JoystickButton1,
        /// <summary>
        /// the press of the touchpad (when hearing a 'click' sound)
        /// </summary>
        TrackpadPress, // = 339, //(int)KeyCode.JoystickButton9, // 339
        /// <summary>
        /// the touch of the touchpad (contact of finger with the touchpad)
        /// </summary>
        TrackpadTouch, // = 347, //(int)KeyCode.JoystickButton17 // 347
        None
    }
    /// <summary>
    /// enumeration of button states
    /// </summary>
    public enum ButtonState
    {
        Down,
        Hold,
        Up
    }
    /// <summary>
    /// enumeration of the default axes on a Google Daydream remote
    /// </summary>
    /// <remarks>axes need to be set up in the InputManager</remarks>
    public enum Axis
    {
        Horizontal,
        Vertical
    }


    private static Dictionary<Button, KeyCode> _dictButtons = new Dictionary<Button, KeyCode>()
    {
#if UNITY_EDITOR
        { Button.App, KeyCode.Alpha1 },{ Button.VolumeUp, KeyCode.UpArrow }, { Button.TrackpadPress, KeyCode.Alpha4 },
        { Button.Home, KeyCode.Alpha2 }, { Button.VolumeDown, KeyCode.DownArrow }, { Button.TrackpadTouch, KeyCode.Alpha3 },
        { Button.None, KeyCode.None }
#else
        { Button.App, KeyCode.JoystickButton6 }, { Button.VolumeUp, KeyCode.JoystickButton0 }, { Button.TrackpadPress, KeyCode.JoystickButton9 },
        { Button.Home, KeyCode.JoystickButton7 }, { Button.VolumeDown, KeyCode.JoystickButton1 }, { Button.TrackpadTouch, KeyCode.JoystickButton17 },
        { Button.None, KeyCode.None }
#endif
    };
    private static Dictionary<Axis, string> _dictAxis = new Dictionary<Axis, string>() { { Axis.Horizontal, "Horizontal" }, { Axis.Vertical, "Vertical" } };

    public static KeyCode MapButton(Button button)
    { return _dictButtons[button]; }
    public static string MapAxis(Axis axis)
    { return _dictAxis[axis]; }





    protected abstract bool Internal_GetButton(MoRSVRInputBase.Button button);
    protected abstract bool Internal_GetButtonDown(MoRSVRInputBase.Button button);
    protected abstract bool Internal_GetButtonUp(MoRSVRInputBase.Button button);


    protected abstract float Internal_GetAxis(MoRSVRInputBase.Axis axis);
    protected abstract float Internal_GetAxisRaw(MoRSVRInputBase.Axis axis);
    protected abstract Vector2 Internal_Trackpad { get; }
    protected abstract Vector2 Internal_TrackpadRaw { get; }

}

public class MoRSVRInput : MoRSVRInputBase
{

    private static MoRSVRInput _instance = null;
    public static MoRSVRInput Instance
    {
        get
        {
            if (_instance == null) _instance = new MoRSVRInput();
            return _instance;
        }
    }



    private MoRSVRInput()
    { }


    protected override bool Internal_GetButton(MoRSVRInputBase.Button button)
    {
        switch (button)
        {
            case MoRSVRInputBase.Button.App:
                return GvrControllerInput.AppButton;
            case MoRSVRInputBase.Button.Home:
                return GvrControllerInput.HomeButtonState;
            case MoRSVRInputBase.Button.TrackpadPress:
                return GvrControllerInput.ClickButton;
            case MoRSVRInputBase.Button.TrackpadTouch:
                return GvrControllerInput.IsTouching;
            case MoRSVRInputBase.Button.VolumeDown:
            case MoRSVRInputBase.Button.VolumeUp:
                return Input.GetKey(MoRSVRInputBase.MapButton(button));
            case MoRSVRInputBase.Button.None:
                return false;
        }
        return false;
    }
    protected override bool Internal_GetButtonDown(MoRSVRInputBase.Button button)
    {
        switch (button)
        {
            case MoRSVRInputBase.Button.App:
                return GvrControllerInput.AppButtonDown;
            case MoRSVRInputBase.Button.Home:
                return GvrControllerInput.HomeButtonDown;
            case MoRSVRInputBase.Button.TrackpadPress:
                return GvrControllerInput.ClickButtonDown;
            case MoRSVRInputBase.Button.TrackpadTouch:
                return GvrControllerInput.TouchDown;
            case MoRSVRInputBase.Button.VolumeDown:
            case MoRSVRInputBase.Button.VolumeUp:
                return Input.GetKeyDown(MoRSVRInputBase.MapButton(button));
            case MoRSVRInputBase.Button.None:
                return false;
        }
        return false;
    }
    protected override bool Internal_GetButtonUp(MoRSVRInputBase.Button button)
    {
        switch (button)
        {
            case MoRSVRInputBase.Button.App:
                return GvrControllerInput.AppButtonUp;
            case MoRSVRInputBase.Button.TrackpadPress:
                return GvrControllerInput.ClickButtonUp;
            case MoRSVRInputBase.Button.TrackpadTouch:
                return GvrControllerInput.TouchUp;
            case MoRSVRInputBase.Button.Home:
            case MoRSVRInputBase.Button.VolumeDown:
            case MoRSVRInputBase.Button.VolumeUp:
                return Input.GetKeyUp(MoRSVRInputBase.MapButton(button));
            case MoRSVRInputBase.Button.None:
                return false;
        }
        return false;
    }


    protected override float Internal_GetAxis(MoRSVRInputBase.Axis axis)
    {
        if (axis == MoRSVRInputBase.Axis.Horizontal)
            return GvrControllerInput.TouchPosCentered.x;
        else
            return GvrControllerInput.TouchPosCentered.y;
    }
    protected override float Internal_GetAxisRaw(MoRSVRInputBase.Axis axis)
    {
        if (axis == MoRSVRInputBase.Axis.Horizontal)
            return GvrControllerInput.TouchPos.x;
        else
            return GvrControllerInput.TouchPos.y;
    }
    protected override Vector2 Internal_Trackpad
    { get { return GvrControllerInput.TouchPosCentered; } }
    protected override Vector2 Internal_TrackpadRaw
    { get { return GvrControllerInput.TouchPos; } }



    public static bool GetButton(MoRSVRInputBase.Button button)
    { return Instance.Internal_GetButton(button); }
    public static bool GetButtonDown(MoRSVRInputBase.Button button)
    { return Instance.Internal_GetButtonDown(button); }
    public static bool GetButtonUp(MoRSVRInputBase.Button button)
    { return Instance.Internal_GetButtonUp(button); }


    public static float GetAxis(MoRSVRInputBase.Axis axis)
    { return Instance.Internal_GetAxis(axis); }
    public static float GetAxisRaw(MoRSVRInputBase.Axis axis)
    { return Instance.Internal_GetAxisRaw(axis); }
    public static Vector2 Trackpad
    { get { return Instance.Internal_Trackpad; } }
    public static Vector2 TrackpadRaw
    { get { return Instance.Internal_TrackpadRaw; } }

}
