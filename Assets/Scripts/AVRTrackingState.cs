﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class AVRTrackingState : MonoBehaviour
{
    [SerializeField()]
    private GameObject _indicatorPrefab = null;

    [SerializeField()]
    [Range(0.001f, 1.0f)]
    private float _dampening = 0.5f;
    [SerializeField()]
    private float _distance = 5.0f;

    private GameObject _indicator = null;


    protected GameObject Indicator
    {
        get
        {
            if (_indicator == null)
            {
                _indicator = GameObject.Instantiate(_indicatorPrefab);
                _indicator.transform.position = this.transform.position + this.transform.forward * _distance;
                _indicator.SetActive(false);
            }
            return _indicator;
        }
    }
    private void Awake()
    {
        if (_indicatorPrefab == null)
        {
            this.enabled = false;
            return;
        }

        this.ShowIndicator();
        //_indicator = GameObject.Instantiate(_indicatorPrefab);
        //_indicator.transform.position = this.transform.position + this.transform.forward * _distance;
    }

    private void Start()
    {
        // parent the progress indicator to the tracking root node
        this.Indicator.transform.SetParent(AVRTrackingRoot.Instance.transform);
    }


    private void LateUpdate()
    {
        //if (_indicator == null)
        //{
        //    _indicator = GameObject.Instantiate(_indicatorPrefab);
        //    _indicator.transform.position = this.transform.position + this.transform.forward * _distance;
        //}
        if (this.Indicator.activeSelf)
        {
            Vector3 startPos = _indicator.transform.position;
            Vector3 targetPos = this.transform.position + this.transform.forward * _distance;
            this.Indicator.transform.position = Vector3.Lerp(startPos, targetPos, _dampening);
            this.Indicator.transform.forward = -(this.transform.position - _indicator.transform.position).normalized;
        }
    }


    public void HideIndicator()
    {
        this.Indicator.SetActive(false);
    }
    public void ShowIndicator()
    {
        this.Indicator.SetActive(true);
    }

}