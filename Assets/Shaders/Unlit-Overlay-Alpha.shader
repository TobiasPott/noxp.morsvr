// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit alpha-blended shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Unlit/AR Camera Passthrough" {
Properties {
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_MaskTex("Mask (R)", 2D) = "white" {}
	_TintColor("Tint (RGB)", Color) = (1, 1, 1)
	//_HSBC("HSBC", Vector) = (0.5, 0.5, 0.5, 0.5)
}

SubShader {
    Tags {"Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent"}
    LOD 100

    ZWrite Off
	ZTest Always
    Blend SrcAlpha OneMinusSrcAlpha

    Pass {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"

			inline float3 applyHue(float3 aColor, float aHue)
			{
				float angle = radians(aHue);
				float3 k = float3(0.57735, 0.57735, 0.57735);
				float cosAngle = cos(angle);
				//Rodrigues' rotation formula
				return aColor * cosAngle + cross(k, aColor) * sin(angle) + k * dot(k, aColor) * (1 - cosAngle);
			}


			inline float4 applyHSBEffect(float4 startColor, fixed4 hsbc)
			{
				float _Hue = 360 * hsbc.r;
				float _Brightness = hsbc.g * 2 - 1;
				float _Contrast = hsbc.b * 2;
				float _Saturation = hsbc.a * 2;

				float4 outputColor = startColor;
				outputColor.rgb = applyHue(outputColor.rgb, _Hue);
				outputColor.rgb = (outputColor.rgb - 0.5f) * (_Contrast)+0.5f;
				outputColor.rgb = outputColor.rgb + _Brightness;
				float3 intensity = dot(outputColor.rgb, float3(0.299,0.587,0.114));
				outputColor.rgb = lerp(intensity, outputColor.rgb, _Saturation);

				return outputColor;
			}

            struct appdata_t {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                float2 texcoord : TEXCOORD0;
				float2 texcoordMask : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };

			sampler2D _MainTex;
			sampler2D _MaskTex;
			float4 _MainTex_ST;
			float4 _MaskTex_ST;
			float4 _TintColor;
			//float4 _HSBC;

            v2f vert (appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.texcoordMask = TRANSFORM_TEX(v.texcoord, _MaskTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.texcoord);
				fixed greyscale = dot(col.rgb, float3(0.3, 0.59, 0.11));
				col.rgb = fixed3(greyscale, greyscale, greyscale);
				//col = applyHSBEffect(col, _HSBC);
				col.a *= tex2D(_MaskTex, i.texcoordMask).r;
				col.rgb *= _TintColor.rgb;

                return col;
            }
        ENDCG
    }
}

}
