﻿// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "NoXP/Unlit World Normals"
{
	Properties
	{
		[HideInInspector]
		_MainTex("Albedo", 2D) = "black" {}
		_BumpScale("BumpScale", Float) = 1.0
		_BumpMap("BumpMap", 2D) = "bump" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		inline fixed4 LightingUnlit(SurfaceOutput s, UnityGI gi)
		{
			return fixed4(s.Albedo, s.Alpha);
		}
		inline half4 LightingUnlit_Deferred(SurfaceOutput s, UnityGI gi, out half4 outGBuffer0, out half4 outGBuffer1, out half4 outGBuffer2)
		{
			UnityStandardData data;
			data.diffuseColor = fixed3(0, 0, 0);
			data.occlusion = 1;
			data.specularColor = 0;
			data.smoothness = 0;
			data.normalWorld = s.Normal;

			UnityStandardDataToGbuffer(data, outGBuffer0, outGBuffer1, outGBuffer2);

			return half4(s.Albedo, 1);
		}
		inline void LightingUnlit_GI(SurfaceOutput s, UnityGIInput data, inout UnityGI gi)
		{
			gi = UnityGlobalIllumination(data, 1.0, s.Normal);
		}
		inline fixed4 LightingUnlit_PrePass(SurfaceOutput s, half4 light)
		{
			return fixed4(s.Albedo, s.Alpha);
		}

		#pragma target 3.5
		#pragma surface surf Unlit noshadow vertex:vert 
		struct Input
		{
			float2 uv_MainTex;
			float3 tangentNormal;

			float3 TanToWorld0;
			float3 TanToWorld1;
			float3 TanToWorld2;
		};

		uniform float _BumpScale;
		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;


		float3x3 Inverse3x3(float3x3 input)
		{
			float3 a = input._11_21_31;
			float3 b = input._12_22_32;
			float3 c = input._13_23_33;
			return float3x3(cross(b,c), cross(c,a), cross(a,b)) * (1.0 / dot(a,cross(b,c)));
		}

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float3 ase_worldNormal = UnityObjectToWorldNormal(v.normal);
			float3 ase_worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
			fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
			float3 ase_worldBitangent = cross(ase_worldNormal, ase_worldTangent) * tangentSign;
			float3x3 ase_worldToTangent = float3x3(ase_worldTangent, ase_worldBitangent, ase_worldNormal);
			float3x3 invertVal23 = Inverse3x3(ase_worldToTangent);
			o.TanToWorld0 = invertVal23[0];
			o.TanToWorld1 = invertVal23[1];
			o.TanToWorld2 = invertVal23[2];
			float3 ase_vertexNormal = v.normal.xyz;
			float4 transform26 = mul(unity_ObjectToWorld, float4(ase_vertexNormal, 0.0));
			float3 componentMask27 = transform26.xyz;
			o.tangentNormal = normalize(mul(ase_worldToTangent, componentMask27));
		}

		void surf(Input i , inout SurfaceOutput o)
		{
			float3x3 TToW = float3x3(i.TanToWorld0, i.TanToWorld1, i.TanToWorld2);
			float3 mapNormal = UnpackScaleNormal(tex2D(_BumpMap, i.uv_MainTex), _BumpScale);
			float3 blendNormal = (BlendNormals(mapNormal, i.tangentNormal));
			o.Albedo = (mul(TToW, blendNormal) * 0.5) + 0.5;
			o.Alpha = 1;
		}

	ENDCG
	}

}